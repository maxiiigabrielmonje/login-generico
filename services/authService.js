const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');
const User = require('../models/user');

const login = async (email, password) => {
    try {
        const user = await User.findOne({ where: { email } });
        if (!user) {
            throw new Error('Credenciales inválidas(user)');
        }

        const passwordMatch = await bcrypt.compare(password, user.password);
        if (!passwordMatch) {
            throw new Error('Credenciales inválidas(password)');
        }

        const token = jwt.sign({ userId: user.id }, 'secret_key', { expiresIn: '1h' });
        return token;
    } catch (error) {
        throw new Error('Error en el inicio de sesión');
    }
};

const register = async (email, password) => {
    try {
        const existingUser = await User.findOne({ where: { email } });
        if (existingUser) {
            throw new Error('El usuario ya existe');
        }

        const hashedPassword = await bcrypt.hash(password, 10);
        await User.create({ email, password: hashedPassword });
        return 'Usuario registrado exitosamente';
    } catch (error) {
        throw new Error('Error al registrar el usuario');
    }
};

const logout = () => {
    try {
        // Lógica para invalidar el token aquí
        return 'Sesión cerrada exitosamente';
    } catch (error) {
        throw new Error('Error al cerrar la sesión');
    }
};

module.exports = {
    login,
    register,
    logout
};
