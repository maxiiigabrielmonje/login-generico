const bcrypt = require('bcrypt');
const User = require('../models/user');

const createUser = async (email, password) => {
    try {
        const hashedPassword = await bcrypt.hash(password, 10);
        const user = await User.create({ email, password: hashedPassword });
        return user;
    } catch (error) {
        throw new Error('Error al crear el usuario');
    }
};

const getAllUsers = async () => {
    try {
        const users = await User.findAll();
        return users;
    } catch (error) {
        throw new Error('Error al obtener los usuarios');
    }
};

const getUserById = async (userId) => {
    try {
        const user = await User.findByPk(userId);
        return user;
    } catch (error) {
        throw new Error('Error al obtener el usuario');
    }
};

const updateUser = async (userId, email, password) => {
    try {
        const user = await User.findByPk(userId);
        if (user) {
            await user.update({ email, password });
            return user;
        } else {
            throw new Error('Usuario no encontrado');
        }
    } catch (error) {
        throw new Error('Error al actualizar el usuario');
    }
};

const deleteUser = async (userId) => {
    try {
        const user = await User.findByPk(userId);
        if (user) {
            await user.destroy();
        } else {
            throw new Error('Usuario no encontrado');
        }
    } catch (error) {
        throw new Error('Error al eliminar el usuario');
    }
};

module.exports = {
    createUser,
    getAllUsers,
    getUserById,
    updateUser,
    deleteUser
};
