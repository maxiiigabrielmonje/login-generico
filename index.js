const express = require('express');
const app = express();
const bodyParser = require('body-parser'); // Importa el módulo body-parser
const userRoutes = require('./routes/userRoutes'); // Importa las rutas de usuario
const authRoutes = require('./routes/authRoutes');

// Configuración de body-parser
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

// Asocia las rutas de usuario al prefijo /api
app.use('/api', userRoutes);
app.use('/api', authRoutes);

// Configura el puerto
const PORT = process.env.PORT || 3000;

// Inicia el servidor
app.listen(PORT, () => {
  console.log(`Servidor en ejecución en el puerto ${PORT}`);
});

