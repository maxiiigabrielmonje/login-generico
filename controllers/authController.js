const { validationResult } = require('express-validator');
const authService = require('../services/authService');

const login = async (req, res) => {
    try {
        const { email, password } = req.body;
        const token = await authService.login(email, password);
        res.json({ token });
    } catch (error) {
        res.status(500).json({ error: error.message });
    }
};

const register = async (req, res) => {
    try {
        const { email, password } = req.body;
        const message = await authService.register(email, password);
        res.status(201).json({ message });
    } catch (error) {
        res.status(500).json({ error: error.message });
    }
};

const logout = async (req, res) => {
    try {
        const message = await authService.logout();
        res.json({ message });
    } catch (error) {
        res.status(500).json({ error: error.message });
    }
};

module.exports = {
    login,
    register,
    logout
};
