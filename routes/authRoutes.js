const express = require('express');
const { body } = require('express-validator');
const authController = require('../controllers/authController');

const router = express.Router();

// Validación de campos para registro
const registrationValidation = [
    body('email').isEmail(),
    body('password').isLength({ min: 6, max: 12 })
];

router.post('/login', authController.login);
router.post('/register', registrationValidation, authController.register);
router.post('/logout', authController.logout);

module.exports = router;
